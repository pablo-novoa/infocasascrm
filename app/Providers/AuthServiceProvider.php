<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function (Request $request) {

            // check auth header           
            if(!$request->hasHeader('Authorization')){
                return null;
            }
            // get JWT
            $authHeader = $request->header('Authorization');
            if (preg_match('/Bearer\s((.*)\.(.*)\.(.*))/', $request->header('Authorization'), $bearerToken)) {
                $jwt = $bearerToken[1];
            }
            // validate and throw ok or error
            try {
                if( $jwtDecoded = JWT::decode($jwt ,env('JWT_SECRET') ,array('HS256')) ){
                    $user = User::where('name', $jwtDecoded->data->name)->first();
                    return $user;
                }
            }catch ( \Exception $e ){
                return null;
            }

        });
    }
}
