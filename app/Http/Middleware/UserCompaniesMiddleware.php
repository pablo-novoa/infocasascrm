<?php

namespace App\Http\Middleware;

use Closure;
use App\Company;

class UserCompaniesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->user() != null ) {
            $userCompanies;
            
            if($request->user()->role == 'admin'){
                $userCompanies = new Company;
            }else{
                $companiesIDs = $request->user()->companies->pluck('id')->toArray();
                $userCompanies = Company::whereIn('id', $companiesIDs);
            }

            $request->user()->companies = $userCompanies;
        }
        
        
        return $next($request);
    }
}