<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roleAllowedSlug)
    {
        if ( $request->user() === null ) {
            return response('Unauthorized.', 401);
        }
        
        $userRole = $request->user()->role;

        if( $userRole === $roleAllowedSlug ){
            return $next($request);
        }else{
            return response('Insufficient permissions.', 403);
        }
    }
}