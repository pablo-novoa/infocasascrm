<?php

namespace App\Http\Controllers;

use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller{

    public function __construct() { }

    public function getAll(Request $request){
        return response()->json(User::all(), 200);
    }

    public function login( Request $request, Response $response ){
        
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ]);

        $name = $request->input('name');
        $password = $request->input('password');
        
        $user = User::where('name', $name)->first();
        if( !$user ){ return response()->json(['Error' => 'Invalid User '], 401);  }

        $passIsValid = Hash::check( $password, $user->password );
        if( !$passIsValid ){ return response()->json(['Error' => 'Invalid password'], 401);  }
        
        $time = time();
        $token = array(
            'iat' => $time, 
            'exp' => $time + ( env('JWT_TTL') ), 
            'data' => [ 
                'id' => $user->id, 
                'name' => $user->name,
                'role' => $user->role,
            ]
        );

        $jwt = JWT::encode($token, env('JWT_SECRET'));
        return response()->json( ['token' => $jwt] );
    }

}