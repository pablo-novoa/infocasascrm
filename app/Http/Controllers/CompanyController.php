<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Company;

class CompanyController extends Controller
{
    
    public function __construct() { }

    public function getAll(Request $request){
        return response()->json($request->user()->companies->get());
    }

    public function get(Request $request, $id){
        return response()->json($request->user()->companies->find($id));
    }

    public function allWith5(Request $request){
        $result = $request->user()->companies
            ->withCount('employees')
            ->having('employees_count', '>=', 5);
        return $result;
    }

    public function allWith5_inTech(Request $request){
        $result = $this->allWith5($request)->where('category', 'tech');
        return $result;
    }

    public function create(Request $request){
        $newCompany = Company::create($request->all());
        if(empty($newCompany))
            return response()->json("Error while creating company", 500);
        
        return response()->json($newCompany, 201);
    }

    public function update(Request $request, $id){
        $company = Company::find($id);
        if(empty($company)) return response()->json("Company not found", 404);
        $company->update($request->all());  

        return response()->json($company, 200);
    }

    public function delete(Request $request, $id){
        $company = Company::find($id);
        if(empty($company)) return response()->json("Company not found", 404);
        $company->delete();  

        return response()->json("Company deleted", 200);
    }
}
