<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Employee;

class EmployeeController extends Controller
{
    
    public function __construct() { }

    public function getAll(Request $request, $companyId){
        $company = $request->user()->companies->find($companyId);
        if(empty($company)){
            return response()->json("Company not found", 404);
        }

        $employees = Employee::where('company_id', $company->id)->get();

        return response()->json($employees, 200);
    }

    public function get(Request $request, $companyId, $id){
        $company = $request->user()->companies->find($companyId);
        if(empty($company)) return response()->json("Company not found", 404);

        $employee = Employee::where('company_id', $company->id)->find($id);
        if(empty($employee)) return response()->json("Employee not found", 404);

        return response()->json($employee, 200);
    }

    public function create(Request $request, $companyId){
        $company = $request->user()->companies->find($companyId);
        if(empty($company)) return response()->json("Company not found", 404);
        
        $newEmplData = $request->all();
        $newEmplData['company_id'] = $company->id;
        $newEmployee = Employee::create($newEmplData);

        if(empty($newEmployee))
            return response()->json("Error while creating employee", 500);
        
        return response()->json($newEmployee, 201);
    }

    public function update(Request $request, $companyId, $id){
        $company = $request->user()->companies->find($companyId);
        if(empty($company)) return response()->json("Company not found", 404);

        $employee = Employee::where('company_id', $company->id)->find($id);
        if(empty($employee)) return response()->json("Employee not found", 404);

        $employee->update($request->all());  

        return response()->json($employee, 200);
    }

    public function delete(Request $request, $companyId, $id){
        $company = $request->user()->companies->find($companyId);
        if(empty($company)) return response()->json("Company not found", 404);

        $employee = Employee::where('company_id', $company->id)->find($id);
        if(empty($employee)) return response()->json("Employee not found", 404);

        $employee->delete();  

        return response()->json("employee deleted", 200);
    }



    
}