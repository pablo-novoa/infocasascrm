<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name', 'last_name', 'gender', 'work_area', 'seniority', 'salary', 'company_id'];


    public function company(){
        return $this->belongsTo('App\Company');
    }
}