<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [ 'name', 'category', 'address' ];

    public function employees(){
        return $this->hasMany('App\Employee');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}