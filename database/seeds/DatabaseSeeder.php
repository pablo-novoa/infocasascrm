<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        factory(App\User::class, 1)->states('admin')->create();
        
        factory(App\Company::class, 50)->create()->each(function ($company) {
            for ($i=0; $i <10 ; $i++) 
                $company->employees()->save(factory(App\Employee::class)->make());

            $basicUser = factory(App\User::class, 1)->create();
            $company->users()->attach($basicUser);
        });
    }
}
