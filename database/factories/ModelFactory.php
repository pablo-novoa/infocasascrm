<?php

use Illuminate\Support\Facades\Hash;

/**
 * Users
 */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'role' => 'basic',
        'password' => Hash::make('password')
    ];
});

$factory->state(App\User::class, 'admin', function ($faker) {
    return [
        'name' => 'admin',
        'role' => 'admin',
        'password' => Hash::make('admin')
    ];
});

/**
 * Companies
 */
$factory->define(App\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'category' => $faker->bs,
        'address' => $faker->address
    ];
});

/**
 * Employees
 */
$factory->define(App\Employee::class, function (Faker\Generator $faker) {
    $seniority = ['junior', 'senior'];
    $gender = ['male', 'female', 'other'];
    $workArea = ['accountant', 'hr', 'development'];

    return [
        'name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'gender' => $gender[rand(0,2)],
        'work_area' => $workArea[rand(0,2)],
        'seniority' => $seniority[rand(0,1)],
        'salary' => $faker->numberBetween(16000, 100000)
    ];
});
