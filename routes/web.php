<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('/login', 'UserController@login');


$router->group(['middleware' => 'auth'], function () use ($router) {

    $router->get('/companies', 'CompanyController@getAll');
    $router->get('/companies/{id}', 'CompanyController@get');
    
    $router->group(['prefix' => '/company/{companyId}/employees'], function () use ($router) {
        $router->get('/', 'EmployeeController@getAll');
        $router->get('/{id}', 'EmployeeController@get');
        
        $router->post('/', 'EmployeeController@create');
        $router->put('/{id}', 'EmployeeController@update');
        $router->delete('/{id}', 'EmployeeController@delete');
    });


    $router->group(['middleware' => 'role:admin'], function () use ($router) {
        $router->get('/users', 'UserController@getAll');

        $router->post('/company', 'CompanyController@create');
        $router->put('/company/{id}', 'CompanyController@update');
        $router->delete('/company/{id}', 'CompanyController@delete');
        
    });
    
});